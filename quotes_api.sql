-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 09:02 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quotes_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'science'),
(2, 'social'),
(3, 'programming'),
(4, 'religion'),
(5, 'politics');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(11) NOT NULL,
  `body` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `body`, `user_id`, `category_id`, `date`) VALUES
(1, 'erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio', 9, 4, '04/11/2018'),
(2, 'in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam', 22, 5, '20/05/2018'),
(3, 'vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam', 13, 4, '28/02/2018'),
(4, 'tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac', 28, 1, '15/04/2018'),
(5, 'interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam', 33, 1, '08/07/2018'),
(6, 'et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin', 13, 3, '15/11/2018'),
(7, 'hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget', 3, 1, '21/10/2018'),
(8, 'tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem', 4, 2, '22/03/2018'),
(9, 'semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci', 34, 3, '28/09/2018'),
(10, 'vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi', 49, 3, '10/04/2018'),
(11, 'tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque', 29, 5, '05/07/2018'),
(12, 'lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis', 5, 5, '11/03/2018'),
(13, 'pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices', 12, 3, '28/07/2018'),
(14, 'ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel', 6, 3, '05/02/2018'),
(15, 'nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac', 25, 4, '04/02/2018'),
(16, 'rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci', 5, 5, '23/06/2018'),
(17, 'elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus', 48, 5, '23/05/2018'),
(18, 'vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in', 13, 4, '07/08/2018'),
(19, 'ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut', 40, 1, '25/02/2018'),
(20, 'et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin', 15, 3, '10/11/2018'),
(21, 'nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede', 5, 1, '21/06/2018'),
(22, 'maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo', 9, 5, '15/02/2018'),
(24, 'odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum', 7, 5, '29/11/2018'),
(25, 'quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices', 26, 4, '09/10/2018'),
(26, 'neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam', 48, 3, '27/07/2018'),
(27, 'quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla', 24, 3, '22/05/2018'),
(28, 'ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien', 43, 5, '17/09/2018'),
(29, 'magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean', 43, 2, '09/04/2018'),
(30, 'semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at diam nam tristique', 20, 4, '28/10/2018'),
(31, 'ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut', 32, 3, '14/05/2018'),
(32, 'faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis', 16, 4, '27/04/2018'),
(33, 'vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae', 2, 4, '01/09/2018'),
(34, 'lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis', 18, 2, '17/05/2018'),
(35, 'luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel', 1, 2, '28/03/2018'),
(36, 'volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis', 23, 1, '30/12/2017'),
(37, 'aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam', 30, 5, '05/04/2018'),
(38, 'fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue', 16, 1, '07/08/2018'),
(39, 'odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum', 28, 1, '25/08/2018'),
(40, 'cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet', 9, 1, '21/04/2018'),
(41, 'commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget', 45, 2, '25/05/2018'),
(42, 'sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla', 39, 4, '27/04/2018'),
(43, 'nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida', 23, 2, '14/10/2018'),
(44, 'at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in', 1, 5, '21/07/2018'),
(45, 'nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam', 41, 2, '29/10/2018'),
(46, 'elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla', 49, 4, '24/01/2018'),
(47, 'justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque', 3, 3, '29/03/2018'),
(48, 'eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio', 5, 4, '01/03/2018'),
(49, 'nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla', 24, 2, '16/07/2018'),
(50, 'suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue', 18, 4, '29/06/2018'),
(51, 'viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a', 23, 2, '19/03/2018'),
(52, 'risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a', 35, 4, '30/11/2018'),
(53, 'dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante', 15, 3, '05/11/2018'),
(54, 'libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula', 46, 3, '12/02/2018'),
(55, 'dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus', 48, 3, '16/10/2018'),
(56, 'in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis', 5, 5, '12/07/2018'),
(57, 'eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis ut at', 16, 2, '01/01/2018'),
(58, 'in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis', 13, 4, '19/12/2017'),
(59, 'in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer', 39, 1, '01/05/2018'),
(60, 'enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros', 19, 5, '21/10/2018'),
(61, 'molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa', 15, 5, '12/09/2018'),
(62, 'dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo', 21, 3, '16/01/2018'),
(63, 'vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem', 26, 1, '08/02/2018'),
(64, 'mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus', 38, 5, '15/03/2018'),
(65, 'ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis', 25, 4, '12/02/2018'),
(66, 'erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus', 42, 2, '14/12/2017'),
(67, 'in libero ut massa volutpat convallis morbi odio odio elementum eu', 33, 1, '15/02/2018'),
(68, 'pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum', 44, 3, '26/01/2018'),
(69, 'aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim', 41, 3, '22/04/2018'),
(70, 'pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris', 8, 4, '21/02/2018'),
(71, 'donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim', 7, 1, '21/10/2018'),
(72, 'tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula', 13, 5, '27/12/2017'),
(73, 'in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis', 13, 1, '22/02/2018'),
(74, 'aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate', 46, 2, '13/05/2018'),
(75, 'This is an updated post', 10, 5, '16/11/2018'),
(76, 'habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt', 36, 5, '30/03/2018'),
(77, 'lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie', 45, 5, '10/10/2018'),
(78, 'amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo', 6, 1, '14/12/2017'),
(79, 'purus sit amet nulla quisque arcu libero rutrum ac lobortis vel', 26, 3, '06/11/2018'),
(80, 'varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a', 28, 1, '16/11/2018'),
(81, 'aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero', 17, 3, '24/12/2017'),
(82, 'penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue', 31, 2, '27/04/2018'),
(83, 'maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui', 1, 4, '27/11/2018'),
(84, 'vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis', 10, 4, '16/05/2018'),
(85, 'ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra', 44, 2, '17/07/2018'),
(86, 'habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque', 9, 2, '14/07/2018'),
(87, 'pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit', 17, 5, '12/01/2018'),
(88, 'convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu', 20, 4, '17/01/2018'),
(89, 'erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis', 45, 4, '01/07/2018'),
(90, 'et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut', 39, 3, '13/02/2018'),
(91, 'hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros', 6, 4, '01/10/2018'),
(92, 'sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis', 15, 4, '06/02/2018'),
(93, 'vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget', 30, 2, '25/05/2018'),
(94, 'sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum', 36, 3, '07/01/2018'),
(95, 'eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras', 35, 4, '22/01/2018'),
(96, 'porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices', 25, 5, '12/04/2018'),
(97, 'vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit', 3, 3, '07/11/2018'),
(98, 'blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed', 9, 5, '30/10/2018'),
(99, 'amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit', 41, 2, '22/04/2018'),
(100, 'nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi', 42, 4, '27/08/2018');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plan` varchar(50) NOT NULL DEFAULT 'basic',
  `calls_made` int(11) NOT NULL DEFAULT '0',
  `time_start` varchar(255) NOT NULL DEFAULT '0',
  `time_end` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `plan`, `calls_made`, `time_start`, `time_end`) VALUES
(1, 'Corrie', 'Letteresse', 'corrieletteresse1', 'corrieletteresse1password', 'basic', 0, '0', '0'),
(2, 'Zelda', 'Scamaden', 'zeldascamaden2', 'zeldascamaden2password', 'unlimited', 0, '0', '0'),
(3, 'Cesare', 'Nockolds', 'cesarenockolds3', 'cesarenockolds3password', 'unlimited', 0, '0', '0'),
(4, 'Jeannette', 'Piniur', 'jeannettepiniur4', 'jeannettepiniur4password', 'unlimited', 0, '0', '0'),
(5, 'Cheston', 'Jiles', 'chestonjiles5', 'chestonjiles5password', 'unlimited', 1, '1549270614', '1549357014'),
(6, 'Blakeley', 'Caccavari', 'blakeleycaccavari6', 'blakeleycaccavari6password', 'basic', 0, '0', '0'),
(7, 'Vivian', 'Orys', 'vivianorys7', 'vivianorys7password', 'unlimited', 0, '0', '0'),
(8, 'Caterina', 'Retchless', 'caterinaretchless8', 'caterinaretchless8password', 'unlimited', 0, '0', '0'),
(9, 'Lorette', 'Ludl', 'loretteludl9', 'loretteludl9password', 'basic', 0, '0', '0'),
(10, 'Charleen', 'Worsnap', 'charleenworsnap10', 'charleenworsnap10password', 'basic', 1, '1549270645', '1549357045'),
(11, 'Concettina', 'Lebang', 'concettinalebang11', 'concettinalebang11password', 'unlimited', 0, '0', '0'),
(12, 'Rosabelle', 'Gallear', 'rosabellegallear12', 'rosabellegallear12password', 'basic', 0, '0', '0'),
(13, 'Deanne', 'Haitlie', 'deannehaitlie13', 'deannehaitlie13password', 'unlimited', 0, '0', '0'),
(14, 'Harlie', 'Wagenen', 'harliewagenen14', 'harliewagenen14password', 'unlimited', 0, '0', '0'),
(15, 'Haleigh', 'Tunuy', 'haleightunuy15', 'haleightunuy15password', 'unlimited', 0, '0', '0'),
(16, 'Gael', 'Sallans', 'gaelsallans16', 'gaelsallans16password', 'basic', 0, '0', '0'),
(17, 'Esther', 'Fielden', 'estherfielden17', 'estherfielden17password', 'unlimited', 0, '0', '0'),
(18, 'Jinny', 'Caustick', 'jinnycaustick18', 'jinnycaustick18password', 'unlimited', 0, '0', '0'),
(19, 'Minnie', 'Uman', 'minnieuman19', 'minnieuman19password', 'basic', 0, '0', '0'),
(20, 'Bobbye', 'Skip', 'bobbyeskip20', 'bobbyeskip20password', 'basic', 0, '0', '0'),
(21, 'Tim', 'Novacek', 'timnovacek21', 'timnovacek21password', 'basic', 0, '0', '0'),
(22, 'Inigo', 'Pleasaunce', 'inigopleasaunce22', 'inigopleasaunce22password', 'unlimited', 0, '0', '0'),
(23, 'Benton', 'Crepin', 'bentoncrepin23', 'bentoncrepin23password', 'unlimited', 0, '0', '0'),
(24, 'Debi', 'Kelcey', 'debikelcey24', 'debikelcey24password', 'unlimited', 0, '0', '0'),
(25, 'Jacques', 'Minney', 'jacquesminney25', 'jacquesminney25password', 'unlimited', 0, '0', '0'),
(26, 'Nester', 'Alelsandrovich', 'nesteralelsandrovich26', 'nesteralelsandrovich26password', 'basic', 0, '0', '0'),
(27, 'Ali', 'McCullouch', 'alimccullouch27', 'alimccullouch27password', 'basic', 0, '0', '0'),
(28, 'Meredith', 'Realff', 'meredithrealff28', 'meredithrealff28password', 'unlimited', 0, '0', '0'),
(29, 'Carole', 'Finker', 'carolefinker29', 'carolefinker29password', 'unlimited', 0, '0', '0'),
(30, 'Terese', 'Rymour', 'tereserymour30', 'tereserymour30password', 'basic', 0, '0', '0'),
(31, 'Lenora', 'Linklet', 'lenoralinklet31', 'lenoralinklet31password', 'unlimited', 0, '0', '0'),
(32, 'Jeremie', 'Stiger', 'jeremiestiger32', 'jeremiestiger32password', 'basic', 0, '0', '0'),
(33, 'Philbert', 'Bozier', 'philbertbozier33', 'philbertbozier33password', 'unlimited', 0, '0', '0'),
(34, 'Kylie', 'Ziehm', 'kylieziehm34', 'kylieziehm34password', 'unlimited', 0, '0', '0'),
(35, 'Lonnie', 'Gawen', 'lonniegawen35', 'lonniegawen35password', 'unlimited', 0, '0', '0'),
(36, 'Con', 'Brandrick', 'conbrandrick36', 'conbrandrick36password', 'unlimited', 0, '0', '0'),
(37, 'Stanislaw', 'Matussevich', 'stanislawmatussevich37', 'stanislawmatussevich37password', 'unlimited', 0, '0', '0'),
(38, 'West', 'Emsden', 'westemsden38', 'westemsden38password', 'unlimited', 0, '0', '0'),
(39, 'Emylee', 'Ruller', 'emyleeruller39', 'emyleeruller39password', 'basic', 0, '0', '0'),
(40, 'Mimi', 'Avrahamof', 'mimiavrahamof40', 'mimiavrahamof40password', 'basic', 0, '0', '0'),
(41, 'Ritchie', 'Duinbleton', 'ritchieduinbleton41', 'ritchieduinbleton41password', 'unlimited', 0, '0', '0'),
(42, 'Holden', 'Joye', 'holdenjoye42', 'holdenjoye42password', 'unlimited', 0, '0', '0'),
(43, 'Coleman', 'Derkes', 'colemanderkes43', 'colemanderkes43password', 'basic', 0, '0', '0'),
(44, 'Dayna', 'McIntee', 'daynamcintee44', 'daynamcintee44password', 'unlimited', 0, '0', '0'),
(45, 'Anselm', 'Bustin', 'anselmbustin45', 'anselmbustin45password', 'unlimited', 0, '0', '0'),
(46, 'Anni', 'Kingzett', 'annikingzett46', 'annikingzett46password', 'unlimited', 0, '0', '0'),
(47, 'Callean', 'Lund', 'calleanlund47', 'calleanlund47password', 'basic', 0, '0', '0'),
(48, 'Bary', 'Reye', 'baryreye48', 'baryreye48password', 'unlimited', 0, '0', '0'),
(49, 'Wade', 'St Quenin', 'wadest quenin49', 'wadest quenin49password', 'unlimited', 0, '0', '0'),
(50, 'Alta', 'Bannard', 'altabannard50', 'altabannard50password', 'unlimited', 0, '0', '0'),
(51, '$firstName', '$lastName', '$password', '$username', 'basic', 0, '0', '0'),
(52, '$firstName', '$lastName', '$password', '$username', 'basic', 0, '0', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
