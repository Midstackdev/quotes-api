<?php 

class User {
	private $db;

	public function __construct(Database $db){
		$this->db = $db;
	}

	public function insertUser($query, $parameters, $id){

		if(isset($parameters->first_name) && isset($parameters->last_name) && isset($parameters->password)){

			$firstName = $parameters->first_name;
			$lastName = $parameters->last_name;
			$password = $parameters->password;
			$username = strtolower($firstName). strtolower($lastName). $id;

			$this->db->insertUser($query, $firstName, $lastName, $password, $username);

			return [
				"first_name" => $firstName,
				"last_name" => $lastName,
				"username" => $username,
				"api_key" => base64_encode("$username:$password"),
			];

		}else{

			return -1;
		}
	}


}